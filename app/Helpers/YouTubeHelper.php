<?php

namespace App\Helpers;

use App\Models\Video;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class YouTubeHelper
{

    const PLAYLIST_URL = 'https://www.youtube.com/playlist?list={URL}&disable_polymer=1';
    const VIDEO_URL = 'https://www.youtube.com';
    const VIDEO_INFO_URL = 'http://youtube.com/get_video_info?video_id={VIDEO_ID}';
    const PROXY_LIST = [
        "159.89.49.60:31264",
        "68.183.20.134:40001",
        "159.89.237.201:40001",
        "64.227.6.113:40001",
        "69.252.50.229:1080",
        "207.97.174.134:1080",
        "68.183.26.102:40001",
        "209.222.117.37:1080",
        "96.114.249.37:1080",
        "206.81.3.245:40001",
        "216.144.228.130:15378",
        "98.143.145.29:62354",
    ];
    const USER_AGENTS = [
        'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36',
        'Mozilla/5.0 (Windows NT 5.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
        'Mozilla/5.0 (iPhone; CPU iPhone OS 13_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Mobile/15E148 Safari/604.1',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/605.1.15 (KHTML, like Gecko)',
        'Mozilla/5.0 (iPad; CPU OS 9_3_5 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Mobile/13G36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
    ];

    public static function getContentWithProxy($url, $index = null, &$output = null)
    {
        $proxy = self::PROXY_LIST[$index ?? array_rand(self::PROXY_LIST)];
        if (!is_null($output))
            $output->text('<info>Current proxy: </info>'.$proxy);
        $user_agent = self::USER_AGENTS[array_rand(self::USER_AGENTS)];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_COOKIEFILE, storage_path("cookie.txt"));
        curl_setopt($ch, CURLOPT_COOKIEJAR, storage_path("cookie.txt"));

        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }


    public static function getPlayListInfo($playlist, &$output = null)
    {
        if (self::isUrl($playlist)) {
            $url_raw = parse_url($playlist);
            parse_str($url_raw['query'], $url_data);
            $playlist = $url_data['list'];
        }

        $matches = [];
        $playlist_url = str_replace('{URL}', $playlist, self::PLAYLIST_URL);
        $content = Http::get($playlist_url)->body();

        $page = new \DOMDocument();
        $page->loadHTML(mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8"));

        $xpath = new \DOMXPath($page);
        $title = $xpath->query('//h1[contains(@class, "pl-header-title")]');
        $node_list = $xpath->query('//a[contains(@class, "pl-video-title-link")]/@href');

        for ($node = 0; $node < $node_list->length; $node++) {
            $matches[] = self::VIDEO_URL . $node_list->item($node)->value;
        }

        $title = trim($title->item(0)->textContent);

        $info = [
            'title' => $title,
            'url' => $playlist_url,
            'code' => $playlist,
            'videos' => $matches
        ];

        return $info;
    }

    public static function getVideoId($url): string
    {
        $video_id = '';

        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/\s]{11})%i', $url, $match)) {
            $video_id = $match[1];
        }

        return $video_id;
    }

    public static function getVideoInfo($video_id, &$output = null)
    {

        $url = str_replace('{VIDEO_ID}', $video_id, self::VIDEO_INFO_URL);
        $tried = 0;
        do {
            $data = self::getContentWithProxy($url, null, $output);
            $content = $data['content'];
            $tried++;
            if ($output){
                $output->text('<info>'.$video_id.'</info> - Try number '.$tried);
            }
        } while(!$content && $tried < count(self::PROXY_LIST));
        parse_str($content, $data);
        unset($data['fflags']);
        $data['player_response'] = @json_decode($data['player_response'], true);

        file_put_contents(base_path() . '/data-'.$video_id.'.json', json_encode($data, JSON_PRETTY_PRINT));

        $description = '';
        try {
            $description = trim($data['player_response']['microformat']['playerMicroformatRenderer']['description']['simpleText']);
        } catch (\Exception $exception) {
            Log::notice('Video ' . $video_id . ' not found description');
        }
        $title = 'not parsed';
        try {
            $title = $data['title'] ?? $data['player_response']['videoDetails']['title'];
        } catch (\Exception $exception) {
            Log::notice('Video ' . $video_id . ' not found title');
        }

        $info = null;
        try {
            $info = [
                'youtube_id' => $video_id,
                'title' => $title,
                'url' => self::VIDEO_URL . '/watch?v=' . $video_id,
                'description' => $description,
                'view_count' => (int)$data['player_response']['videoDetails']['viewCount'],
                'duration' => (int)$data['player_response']['videoDetails']['lengthSeconds'],
            ];
        } catch (\Exception $exception) {
            Log::notice('Video ' . $video_id . ' bad description');
        }

        return $info;
    }

    public static function isUrl($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL) || (stristr($url, 'youtube.com') !== false && filter_var('https://' . $url, FILTER_VALIDATE_URL));
    }

    public static function run($playlist, &$output = null)
    {
        $playlist_info = self::getPlayListInfo($playlist);
        if ($output) {
            $playlist_title = Str::slug($playlist_info['title']);
            $output->text("<info>Playlist: </info> {$playlist_title}");
        }
        foreach ($playlist_info['videos'] AS &$item) {
            if ($video = self::getVideoInfo(self::getVideoId($item), $output)) {
                if (empty($video['title'])) {
                    $output->text('Empty title: '.implode(' -- ', $video));
                    continue;
                }
                $item = $video;
                try {
                    $video['playlist'] = $playlist_info['title'];
                    Video::updateOrCreate(['youtube_id' => $video['youtube_id']], $video);
                } catch (\Exception $exception) {
                    Log::error('Update or create video fail', compact('video', 'exception'));
                }
            }
        }
        file_put_contents(base_path() . '/playlist_info.json', json_encode($playlist_info, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
        return $playlist_info;
    }
}