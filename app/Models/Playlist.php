<?php

namespace App\Models;

use App\Traits\CreatedAtOrdered;
use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    use CreatedAtOrdered;
    protected $fillable = ['code', 'title', 'url'];
}
