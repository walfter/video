<?php

namespace App\Models;

use App\Traits\CreatedAtOrdered;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    const CREATED_AT = 'date_create';
    const UPDATED_AT = 'date_update';

    protected $table = 'video';

    protected $dates = [
        'date_create',
        'date_update',
    ];

    protected $fillable = ['youtube_id', 'title', 'url', 'description', 'view_count', 'duration', 'playlist'];

    protected static function boot()
    {
        parent::boot();

        self::updating(function (Video $video) {
            $video->date_update = now();
        });

        self::creating(function (Video $video) {
            $video->date_create = now();
            $video->date_update = now();
        });
    }
}
