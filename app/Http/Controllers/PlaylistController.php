<?php

namespace App\Http\Controllers;

use App\Helpers\YouTubeHelper;
use App\Models\Playlist;
use Illuminate\Http\Request;

class PlaylistController extends Controller
{

    public function index()
    {
        $playlists = Playlist::paginate(config('app.per_page'));
        return view('playlists.index', compact('playlists'));
    }

    public function store(Request $request)
    {
        $playlist_info = YouTubeHelper::getPlayListInfo($request->get('url'));
        Playlist::updateOrCreate(['code' => $playlist_info['code']], [
            'title' => $playlist_info['title'],
            'url' => $playlist_info['url'],
        ]);
        return redirect()->back();
    }

    public function destroy(Playlist $playlist)
    {
        $playlist->delete();
        return redirect()->back();
    }
}
