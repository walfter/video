<?php
/**
 * Created by PhpStorm.
 * User: walft
 * Date: 09.08.2020
 * Time: 19:20
 */

namespace App\Traits;


use App\Scopes\CreatedAtOrderScope;

trait CreatedAtOrdered
{
    private static $orderDirection = 'DESC';

    protected static function bootCreatedAtOrdered() {
        static::addGlobalScope(new CreatedAtOrderScope(self::$orderDirection));
    }
}