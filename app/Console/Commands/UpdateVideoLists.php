<?php

namespace App\Console\Commands;

use App\Helpers\YouTubeHelper;
use App\Models\Playlist;
use App\Models\Video;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class UpdateVideoLists extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'video:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $playlists = Playlist::all();

        $this->output->title("Update video lists");

        foreach ($playlists AS &$playlist) {
            $playlist_info = YouTubeHelper::run($playlist->code, $this->output);

            if (is_null($playlist->title)) {
                $playlist->title = $playlist_info['title'];
                $playlist->save();
            }
        }
    }
}
