<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('youtube_id')->nullable()->unique()->index();
            $table->string('title');
            $table->string('url')->unique()->index();
            $table->text('description')->nullable();
            $table->integer('status')->default(0);
            $table->string('playlist', 511);
            $table->dateTime('date_create')->nullable();
            $table->dateTime('date_update')->nullable();
            $table->integer('likes')->default(0);
            $table->boolean('use_youtube_info')->default(true);
            $table->boolean('show_index')->default(false);
            $table->integer('view_count')->default(0);
            $table->integer('duration')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video');
    }
}
