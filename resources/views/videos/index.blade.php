@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Видеозаписи</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Заголовок</th>
                            <th>Код</th>
                            <th>Ссылка</th>
                            <th>Описание</th>
                            <th>Длительность</th>
                            <th>Количество просмотров</th>
                            <th>Плейлист</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($videos AS $video)
                            <tr>
                                <td>{{ $video->id }}</td>
                                <td>{{ $video->title }}</td>
                                <td>{{ $video->youtube_id }}</td>
                                <td>{{ $video->url }}</td>
                                <td>{{ $video->description }}</td>
                                <td>{{ $video->duration }}</td>
                                <td>{{ $video->view_count }}</td>
                                <td>{{ $video->playlist }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $videos->links() }}
            </div>
        </div>
    </div>
@stop