@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Плейлисты</div>
            <div class="card-body">
                <form action="{{ route('playlists.store') }}" method="POST">
                    @csrf
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Ссылка</span>
                        </div>
                        <input type="text" class="form-control" placeholder="Ссылка на плейлист" aria-label="Username"
                               aria-describedby="basic-addon1" name="url">
                        <div class="input-group-append">
                            <button class="btn btn-outline-success" type="submit">Сохранить</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Код</th>
                            <th>Заголовок</th>
                            <th>Ссылка</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($playlists AS $playlist)
                            <tr>
                                <td>{{ $playlist->id }}</td>
                                <td>{{ $playlist->code }}</td>
                                <td>{{ $playlist->title }}</td>
                                <td>{{ $playlist->url }}</td>
                                <td>
                                    <form action="{{ route('playlists.destroy', $playlist) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-sm">Удалить</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $playlists->links() }}
            </div>
        </div>
    </div>
@stop